<?php

Route::group(['namespace' => 'StoneyEagle\Slideshow\Http\Controllers'], function () {
    Route::get('/slideshow',     'PexelsController@slideshow')->name('slideshow');
});
