<?php

namespace StoneyEagle\Slideshow\Http\Controllers;

use App\Http\Controllers\Controller;

class PexelsController extends Controller
{
    public function slideshow()
    {
        return view('pexels::slideshow');
    }
}
