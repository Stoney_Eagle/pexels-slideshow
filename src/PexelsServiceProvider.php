<?php

namespace StoneyEagle\Slideshow;

use Illuminate\Support\ServiceProvider;

class PexelsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'pexels');
    }
    public function register()
    { }
}
